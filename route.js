 Router.configure({
   layoutTemplate: 'layout'  //can be any template name
 });

Router.map(function () {
//  this.route('frontpage');
  this.route('portfolio');
  this.route('blog');
  this.route('documentation');
  this.route('contact');
  this.route('login');
  this.route('post');
  
  this.route('frontpage', {
    path: '/'
  });
});
