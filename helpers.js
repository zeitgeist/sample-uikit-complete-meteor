if (Meteor.isClient) {
    Template.navItems.helpers({
    activeIfTemplateIs: function (template) {
    var currentRoute = Router.current();
    return currentRoute && template === currentRoute.lookupTemplate() ? 'uk-active' : '';
    }
    });
    
    Meteor.startup(function() {
    $('body').attr('data-uk-observe', '1');
    });
}